/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Graduate
 */
public class LoginDAO {

    public static boolean validate(LoginBean bean) {
        boolean status = false;
        try {
            Connection con = DBConnector.getConnector().getConnection();
            
            String userTableName = "db_grad_cs_1917.users";

            PreparedStatement ps = con.prepareStatement(
                    "select * from db_grad_cs_1917.users where user_id=? and user_pwd=?");

            ps.setString(1, bean.getUser());
            ps.setString(2, bean.getPass());
            System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();
            status = rs.next();

        } catch (Exception e) {
        }

        return status;

    }

}
