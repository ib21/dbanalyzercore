package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import domain.Instrument;
import domain.InstrumentStat;
import java.sql.CallableStatement;
import javafx.util.Pair;

public class InstrumentDAO
{

    Connection con = null;

    private static String instrumentTableName = "instrument";

    public ArrayList<Instrument> getAllInstruments()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all links from the instrument table
        String sql = "select * from " + instrumentTableName;
        ResultSet result;
        
        // Declare a collection of Instrument
        ArrayList<Instrument> instruments = new ArrayList();
        try
        {
            Instrument inst;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	inst = new Instrument(result.getInt(1), result.getString(2));
            	instruments.add(inst);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            instruments = null;
        } 
        return instruments;
    }
    
    public ArrayList<InstrumentStat> getInstrumentStatistics()
    {
        PreparedStatement selectStmt = null;

        String sql ="select 	instrument.instrument_name,\n" +
                    "	    avg_sell_price, \n" +
                    "        avg_buy_price\n" +
                    "from\n" +
                    "(select \n" +
                    "	    A.deal_instrument_id,\n" +
                    "        A.avg_price as avg_sell_price, \n" +
                    "        B.avg_price as avg_buy_price\n" +
                    "	from\n" +
                    "	(select \n" +
                    "			deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "\n" +
                    "            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price\n" +
                    "	from deal \n" +
                    "	group by deal_instrument_id, deal_type) A,\n" +
                    "    \n" +
                    "	(select \n" +
                    "			deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price\n" +
                    "	from deal \n" +
                    "	group by  deal_instrument_id, deal_type) B \n" +
                    "    \n" +
                    "where A.deal_instrument_id = B.deal_instrument_id\n" +
                    "      and A.deal_type = \"S\" and B.deal_type = \"B\" ) T\n" +
                    "      inner join instrument\n" +
                    "where instrument.instrument_id = T.deal_instrument_id";
        ResultSet result;
        
        // Declare a collection of InstrumentStat
        ArrayList<InstrumentStat> stats = new ArrayList();
        try
        {
            InstrumentStat stat;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
                stat = new InstrumentStat(result.getString(1), 
                    result.getDouble(2), result.getDouble(3));
                stats.add(stat);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            stats = null;
        } 
        return stats;
    }
    
    
    public ArrayList<Pair> getBuyQuantity()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all realized profits for each party
        String sql ="select  instrument.instrument_name,\n" +
                    "		A.quantity as sell_quantity,\n" +
                    "        B.quantity as buy_quantity\n" +
                    "	from\n" +
                    "	(select deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "            sum(deal_quantity) as quantity\n" +
                    "	from deal \n" +
                    "	group by deal_instrument_id, deal_type) A\n" +
                    "    inner join\n" +
                    "	(select deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_quantity) as quantity\n" +
                    "	from deal \n" +
                    "	group by deal_instrument_id, deal_type) B \n" +
                    "    inner join instrument\n" +
                    "    \n" +
                    "where A.deal_instrument_id = B.deal_instrument_id\n" +
                    "      and A.deal_type = \"S\" and B.deal_type = \"B\"\n" +
                    "      and instrument.instrument_id = A.deal_instrument_id;";
        ResultSet result;
        
        // Declare a collection of pairs
        ArrayList<Pair> bQuantity = new ArrayList();
        try
        {
            Pair<String, Integer> pair;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	pair = new Pair(result.getString(1), result.getInt(3));
            	bQuantity.add(pair);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            bQuantity = null;
        } 
        return bQuantity;
    }
    
    public ArrayList<Pair> getSellQuantity()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all realized profits for each party
        String sql ="select  instrument.instrument_name,\n" +
                    "		A.quantity as sell_quantity,\n" +
                    "        B.quantity as buy_quantity\n" +
                    "	from\n" +
                    "	(select deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "            sum(deal_quantity) as quantity\n" +
                    "	from deal \n" +
                    "	group by deal_instrument_id, deal_type) A\n" +
                    "    inner join\n" +
                    "	(select deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_quantity) as quantity\n" +
                    "	from deal \n" +
                    "	group by deal_instrument_id, deal_type) B \n" +
                    "    inner join instrument\n" +
                    "    \n" +
                    "where A.deal_instrument_id = B.deal_instrument_id\n" +
                    "      and A.deal_type = \"S\" and B.deal_type = \"B\"\n" +
                    "      and instrument.instrument_id = A.deal_instrument_id;";
        ResultSet result;
        
        // Declare a collection of pairs
        ArrayList<Pair> sQuantity = new ArrayList();
        try
        {
            Pair<String, Integer> pair;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	pair = new Pair(result.getString(1), result.getInt(2));
            	sQuantity.add(pair);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            sQuantity = null;
        } 
        return sQuantity;
    }
}
