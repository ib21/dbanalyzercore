package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import domain.Counterparty;
import java.sql.CallableStatement;
import javafx.util.Pair;

public class CounterpartyDAO
{

    Connection con = null;

    private static String counterpartyTableName = "counterparty";

    public ArrayList<Counterparty> getAllCounterparties()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all links from the counterparty table
        String sql = "select * from " + counterpartyTableName;
        ResultSet result;
        
        // Declare a collection of Instrument
        ArrayList<Counterparty> cparties = new ArrayList();
        try
        {
            Counterparty cparty;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	cparty = new Counterparty(result.getInt(1), result.getString(2), 
                    result.getString(3), result.getString(4));
            	cparties.add(cparty);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            cparties = null;
        } 
        return cparties;
    }
    
    public ArrayList<Pair> getRealizedProfitLoss()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all realized profits for each party
        String sql ="select 	counterparty.counterparty_name,\n" +
                    "	    net_trades_money\n" +
                    "from\n" +
                    "(select A.deal_counterparty_id, \n" +
                    "		(A.money - B.money) as net_trades_money\n" +
                    "	from\n" +
                    "	(select deal_counterparty_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_amount * deal_quantity) as money\n" +
                    "	from deal \n" +
                    "	group by deal_counterparty_id, deal_type) A,\n" +
                    "    \n" +
                    "	(select deal_counterparty_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_amount * deal_quantity) as money\n" +
                    "	from deal \n" +
                    "	group by deal_counterparty_id, deal_type) B \n" +
                    "    \n" +
                    "where A.deal_counterparty_id = B.deal_counterparty_id \n" +
                    "      and A.deal_type = \"S\" and B.deal_type = \"B\") T\n" +
                    "inner join counterparty\n" +
                    "where counterparty.counterparty_id = T.deal_counterparty_id";
        ResultSet result;
        
        // Declare a collection of pairs
        ArrayList<Pair> rProfits = new ArrayList();
        try
        {
            Pair<String, Double> pair;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	pair = new Pair(result.getString(1), result.getDouble(2));
            	rProfits.add(pair);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            rProfits = null;
        } 
        return rProfits;
    }
    
    public ArrayList<Pair> getEffectiveProfitLoss()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all realized profits for each party
        String sql ="select Dealer.counterparty_name,\n" +
                    "       sum(Dealer.profit) as \"profit/loss\"\n" +
                    "from\n" +
                    "(select  counterparty.counterparty_name, \n" +
                    "	    instrument.instrument_name,\n" +
                    "		MainTab.net_trades_money,\n" +
                    "		MainTab.net_trades_quantity,\n" +
                    "        MainTab.avg_sell_price, \n" +
                    "        MainTab.avg_buy_price,\n" +
                    "        Price_tab.last_sell_price,\n" +
                    "        Price_tab.last_buy_price,\n" +
                    "        case \n" +
                    "			when MainTab.net_trades_quantity < 0 then\n" +
                    "            MainTab.net_trades_money - \n" +
                    "            MainTab.net_trades_quantity * (Price_tab.last_sell_price - MainTab.avg_buy_price)\n" +
                    "            else \n" +
                    "            MainTab.net_trades_money + \n" +
                    "            MainTab.net_trades_quantity * (Price_tab.last_sell_price - MainTab.avg_buy_price)\n" +
                    "		end as profit\n" +
                    "from\n" +
                    "(select A.deal_counterparty_id, \n" +
                    "	    A.deal_instrument_id,\n" +
                    "		(A.money - B.money) as net_trades_money,\n" +
                    "		(A.quantity - B.quantity) as net_trades_quantity,\n" +
                    "        A.avg_price as avg_sell_price, \n" +
                    "        B.avg_price as avg_buy_price\n" +
                    "	from\n" +
                    "	(select deal_counterparty_id,\n" +
                    "			deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_amount * deal_quantity) as money,\n" +
                    "            sum(deal_quantity) as quantity,\n" +
                    "            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price\n" +
                    "	from deal \n" +
                    "	group by deal_counterparty_id, deal_instrument_id, deal_type) A,\n" +
                    "    \n" +
                    "	(select deal_counterparty_id,\n" +
                    "			deal_instrument_id,\n" +
                    "			deal_type,\n" +
                    "			sum(deal_amount * deal_quantity) as money,\n" +
                    "            sum(deal_quantity) as quantity,\n" +
                    "            sum(deal_amount * deal_quantity)/sum(deal_quantity) as avg_price\n" +
                    "	from deal \n" +
                    "	group by deal_counterparty_id, deal_instrument_id, deal_type) B \n" +
                    "    \n" +
                    "where A.deal_counterparty_id = B.deal_counterparty_id \n" +
                    "	  and A.deal_instrument_id = B.deal_instrument_id\n" +
                    "      and A.deal_type = \"S\" and B.deal_type = \"B\") MainTab\n" +
                    "      \n" +
                    "inner join\n" +
                    "\n" +
                    "(select Price.deal_counterparty_id,\n" +
                    "	   Price.deal_instrument_id,\n" +
                    "	   Price.deal_type,\n" +
                    "	   Price.last_time,\n" +
                    "       Price.last_price as last_sell_price,\n" +
                    "       Price2.last_price as last_buy_price\n" +
                    "from\n" +
                    "(select PriceGr.deal_counterparty_id,\n" +
                    "	   PriceGr.deal_instrument_id,\n" +
                    "	   PriceGr.deal_type,\n" +
                    "	   max(PriceGr.last_time) as last_time,\n" +
                    "       max(PriceGr.last_price) as last_price\n" +
                    "from\n" +
                    "(select LastDate.deal_counterparty_id,\n" +
                    "	   LastDate.deal_instrument_id,\n" +
                    "	   LastDate.deal_type,\n" +
                    "	   LastDate.last_time,\n" +
                    "       deal_amount as last_price\n" +
                    "from\n" +
                    "(select  deal_counterparty_id\n" +
                    "		, deal_instrument_id\n" +
                    "		, deal_type\n" +
                    "        , max(deal_time) as last_time\n" +
                    "	from deal  \n" +
                    "    group by deal_counterparty_id, deal_instrument_id, deal_type) LastDate\n" +
                    "inner join deal\n" +
                    "where LastDate.deal_counterparty_id = deal.deal_counterparty_id \n" +
                    "	  and LastDate.deal_instrument_id = deal.deal_instrument_id\n" +
                    "      and LastDate.deal_type = deal.deal_type\n" +
                    "      and last_time = deal.deal_time\n" +
                    "order by deal_counterparty_id, deal_instrument_id, deal_type) PriceGr\n" +
                    "group by PriceGr.deal_counterparty_id, PriceGr.deal_instrument_id, PriceGr.deal_type) Price,\n" +
                    "\n" +
                    "(select PriceGr.deal_counterparty_id,\n" +
                    "	   PriceGr.deal_instrument_id,\n" +
                    "	   PriceGr.deal_type,\n" +
                    "	   max(PriceGr.last_time) as last_time,\n" +
                    "       max(PriceGr.last_price) as last_price\n" +
                    "from\n" +
                    "(select LastDate.deal_counterparty_id,\n" +
                    "	   LastDate.deal_instrument_id,\n" +
                    "	   LastDate.deal_type,\n" +
                    "	   LastDate.last_time,\n" +
                    "       deal_amount as last_price\n" +
                    "from\n" +
                    "(select  deal_counterparty_id\n" +
                    "		, deal_instrument_id\n" +
                    "		, deal_type\n" +
                    "        , max(deal_time) as last_time\n" +
                    "	from deal  \n" +
                    "    group by deal_counterparty_id, deal_instrument_id, deal_type) LastDate\n" +
                    "inner join deal\n" +
                    "where LastDate.deal_counterparty_id = deal.deal_counterparty_id \n" +
                    "	  and LastDate.deal_instrument_id = deal.deal_instrument_id\n" +
                    "      and LastDate.deal_type = deal.deal_type\n" +
                    "      and last_time = deal.deal_time\n" +
                    "order by deal_counterparty_id, deal_instrument_id, deal_type) PriceGr\n" +
                    "group by PriceGr.deal_counterparty_id, PriceGr.deal_instrument_id, PriceGr.deal_type\n" +
                    ") Price2\n" +
                    "\n" +
                    "where Price.deal_counterparty_id = Price2.deal_counterparty_id \n" +
                    "	  and Price.deal_instrument_id = Price2.deal_instrument_id\n" +
                    "      and Price.deal_type = \"S\" and Price2.deal_type = \"B\") Price_tab\n" +
                    "\n" +
                    "inner join counterparty inner join instrument\n" +
                    "\n" +
                    "where MainTab.deal_counterparty_id = Price_tab.deal_counterparty_id \n" +
                    "	  and MainTab.deal_instrument_id = Price_tab.deal_instrument_id\n" +
                    "      and MainTab.deal_counterparty_id = counterparty.counterparty_id\n" +
                    "      and MainTab.deal_instrument_id = instrument.instrument_id\n" +
                    "        \n" +
                    ") Dealer\n" +
                    "group by counterparty_name;";
        ResultSet result;
        
        // Declare a collection of pairs
        ArrayList<Pair> eProfits = new ArrayList();
        try
        {
            Pair<String, Double> pair;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	pair = new Pair(result.getString(1), result.getDouble(2));
            	eProfits.add(pair);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            eProfits = null;
        } 
        return eProfits;
    }


}
