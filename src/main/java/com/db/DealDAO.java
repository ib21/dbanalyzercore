package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import domain.Deal;
import java.sql.CallableStatement;
import javafx.util.Pair;

public class DealDAO
{

    Connection con = null;

    private static String dealTableName = "deal";
    //private static String joiningTable = "linkToMetatag";
   // private static String tagTableName = "metatag";

//    private int getCurrentIDNum()
//    {
//        int result = 0;
//        try
//        {
//            Statement st = con.createStatement();
//            ResultSet rs = st.executeQuery("select link_id from " + linkTableName);
//            while (rs.next())
//            {
//                int idNum = rs.getInt("link_id");
//                if (idNum > result)
//                {
//                    result = idNum;
//                }
//            }
//        } catch (SQLException se)
//        {
//            System.out.println(se);
//        }
//        return result;
//    }

    public ArrayList<Deal> getAllDeals()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all links from the deal table
        String sql = "select * from " + dealTableName;
        ResultSet result;
        
        // Declare a collection of Deal
        ArrayList<Deal> deals = new ArrayList();
        try
        {
            Deal deal;
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery();
            
            while (result.next())
            {
            	deal = new Deal(result.getInt(1), result.getString(2), 
                    result.getInt(3), result.getInt(4), result.getString(5),
                    result.getDouble(6), result.getInt(7));
            	deals.add(deal);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            deals = null;
        } 
        return deals;
    }

//    public ArrayList<Bookmark> getLinksForTag(String metatag)
//    {
//    	Bookmark bookmark;
//        String sql = "SELECT * FROM " + linkTableName + " INNER JOIN " + joiningTable + " ON " + joiningTable + ".link_id = " + linkTableName
//                + ".link_id where " + joiningTable + ".tag_name = ?";
//        ResultSet result;
//        PreparedStatement selectStmt = null;
//        ArrayList<Bookmark> bookmarks = new ArrayList<>();
//        try
//        {
//            con = DBConnector.getConnector().getConnection();
//            selectStmt = con.prepareStatement(sql);
//            selectStmt.setString(1, metatag);
//            result = selectStmt.executeQuery();
//            while (result.next())
//            {
//            	bookmark = new Bookmark(result.getInt(1), result.getString(2), result.getString(3));
//                bookmarks.add(bookmark);
//            }
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//            bookmarks = null;
//        } 
//        return bookmarks;
//    }
}
