package domain;

public class Deal {
	private int id;
	private String time;
	private int counterId;
	private int instrumentId;
	private String type;
	private double amount;
	private int quantity;
	
	public Deal (int id, String time, int counterId, int instrumentId, String type, double amount, int quantity)
	{
		this.id = id;
		this.time = time;
		this.counterId = counterId;
		this.instrumentId = instrumentId;
		this.type = type;
		this.amount = amount;
		this.quantity = quantity;
	}

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @return the counterId
     */
    public int getCounterId() {
        return counterId;
    }

    /**
     * @return the instrumentId
     */
    public int getInstrumentId() {
        return instrumentId;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }
	

}
