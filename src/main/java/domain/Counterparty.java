/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Graduate
 */
public class Counterparty 
{
    private int id;
    private String name;
    private String status;
    private String date;
    
    public Counterparty (int id, String name, String status, String date)
    {
        this.id = id;
        this.name = name;
        this.status = status;
        this.date = date;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }
    
    
}
