/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Graduate
 */
public class InstrumentStat {
    
    private String name;
    private double averageB;
    private double averageS;
    
    public InstrumentStat(String name, double averageB, double averageS) 
    {
        this.name = name;
        this.averageB = averageB;
        this.averageS = averageS;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the averageB
     */
    public double getAverageB() {
        return averageB;
    }

    /**
     * @return the averageS
     */
    public double getAverageS() {
        return averageS;
    }
    
}
